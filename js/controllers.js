function SamplesControl($scope) {
	
	$scope.this = this;
	var haveStage = false;	
	var stage = undefined;
	var canvas;
	var stage;
	var container;
	var captureContainers;
	var captureIndex;
	
	$scope.onClickStartCanvas = function(){
		var vid = $('#StrobeMediaPlayback');
		var vidpos = vid.position();
		$('#stage').css({
			top: vidpos.top,
			left: vidpos.left,
			height: vid.height()-33
		}).show();
		$scope.initOverlay();
	}
	
	$scope.initOverlay = function() {
		// create a new stage and point it at our canvas:
		canvas = $('#stage')[0]; // We can't use the jQuery object here, we need the HTML object
		stage = new Stage(canvas);

		var w = canvas.width;
		var h = canvas.height;

		container = new Container();
		stage.addChild(container);

		captureContainers = [];
		captureIndex = 0;

		// create a large number of slightly complex vector shapes, and give them random positions and velocities:
		for (var i=0; i<50; i++) {
			var obj = new Shape();
			obj.graphics.beginFill(Graphics.getHSL(Math.random()*1000-45, 100, 50+Math.random()*90));
			obj.graphics.drawCircle(0,0,Math.random()*20);
			
			obj.y = -100;

			container.addChild(obj);
		}

		for (i=0; i<100; i++) {
			var captureContainer = new Container();
			captureContainer.cache(0,0,w,h);
			captureContainers.push(captureContainer);
		}

		// start the tick and point it at the window so we can do some work before updating the stage:
		Ticker.addListener($scope);
		Ticker.setFPS(30);
	}

	$scope.tick = function() {
		var w = canvas.width;
		var h = canvas.height;
		var l = container.getNumChildren();

		captureIndex = (captureIndex+1)%captureContainers.length;
		stage.removeChildAt(0);
		var captureContainer = captureContainers[captureIndex];
		stage.addChildAt(captureContainer,0);
		captureContainer.addChild(container);

		// iterate through all the children and move them according to their velocity:
		for (var i=0; i<l; i++) {
			var heart = container.getChildAt(i);
			if (heart.y < -50) {
				heart._x = Math.random()*w;
				heart.y = h*(1+Math.random())+50;
				heart.perX = (1+Math.random())*h;
				heart.offX = Math.random()*h;
				heart.ampX = heart.perX*0.1*(0.2+Math.random());
				heart.velY = -Math.random()*2-2;
				heart.scaleX = heart.scaleY = Math.random()+1;
				heart.rotation = Math.random()*40-20;
				heart.alpha = Math.random();
			}
			heart.y += heart.velY;
			heart.x = heart._x + Math.cos((heart.offX+heart.y)/heart.perX*Math.PI*2)*heart.ampX;
		}

		captureContainer.updateCache("source-over");

		// draw the updates to stage:
		stage.update();
	}
	
	var divRunning = false;
	$scope.onClickStartDiv = function(){
		
		if(!divRunning) {
			divRunning = true;
			
			var vid = $('#StrobeMediaPlayback');
			var mark = $('#mark');
			vidPos = vid.position();
			mark.css({
				top: vidPos.top,
				left: vidPos.left
			}).show();
			
			mark.animate({
				top:"+=400",
				left:"+=550"
			}, 3000, function(){
				mark.animate({
					top:"-=400",
					left:"-=550"
				}, 3000, function(){
					mark.hide();
					divRunning = false;
				});
			});
		}
	}
	
	
	
}